$(function() {
	var selectedClass = "";
	$(".filter").click(function(){//choose in search filter
		selectedClass = $(this).attr("data-rel");//Class has selected
		$("#gallery .container-fluid .row  div").not("."+selectedClass).hide();//Hidden element no choose
		setTimeout(function(){
			$("."+selectedClass).show(5);//Show element follow selectedClass
			$("."+selectedClass + " .image").show(5);
			$("."+selectedClass + " .image .img-hover").show(5);
			$("."+selectedClass + " .image .img-hover .search-icon").show(5);
		},10);
	});
});