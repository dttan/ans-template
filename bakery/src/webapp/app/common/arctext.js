
var $example1	= $('.logo').hide();
var $example2	= $('#new-products-title').hide();
var $example3	= $('#example3').hide();
var $example4	= $('#example4').hide();

google.load('webfont','1');

google.setOnLoadCallback(function() {
    WebFont.load({
        google		: {
            families	: ['Montserrat','Concert One']
        },
        fontactive	: function(fontFamily, fontDescription) {
            init();
        },
        fontinactive	: function(fontFamily, fontDescription) {
            init();
        }
    });
});

function init() {    
    $example1.show().arctext({radius: 300});
    $example2.show().arctext({radius: 200, dir: -1});
    $example3.show().arctext({radius: 500, rotate: false});
    $example4.show().arctext({radius: 300});
};