const sass = "app/scss/**/*.scss";
const assets = {
    css: "app/assets/css",
    lib: "app/assets/css/lib",
    fonts: {
        fonts:"app/assets/fonts",
        fontawesome:"app/assets/fonts/fontawesome",
        pacifico:"app/assets/fonts/pacifico",
        nunito:"app/assets/fonts/nunito"
    },
    lang: "app/assets/lang",
    images: "app/assets/images"
}
const pug = {
    src: "app/pug/**/*.pug",
    html: "app/html"
};

const common = {
    lib: "app/common/lib",
    common:"app/common"
}
module.exports = {
    sass: sass,
    assets: assets,
    pug: pug,
    common:common
  };