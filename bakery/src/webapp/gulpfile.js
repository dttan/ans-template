let gulp = require('gulp');
let sass = require('gulp-sass');
let pug = require('gulp-pug');
let browserSync = require('browser-sync').create();
let resource = require('./app/common/const.js');

/** Preprocess from scss to css */
gulp.task('sass', (callback)=>{
    gulp.src(resource.sass)
        .pipe(sass())
        .pipe(gulp.dest(resource.assets.css))
        .pipe(browserSync.stream());
	callback();
});
/** Preprocess from pug to html */
gulp.task('pug', (callback)=>{
    gulp.src(resource.pug.src)
        .pipe(pug({pretty:true}))
        .pipe(gulp.dest(resource.pug.html))
        .pipe(browserSync.stream());
    callback();
});
/** Create index page */
gulp.task('index', (callback)=>{
	gulp.src('app/html/index.html')
		.pipe(gulp.dest('app'));
	callback();
});
/** Follow file if change is resfresh browser */
gulp.task('watch', (callback)=>{
    gulp.watch(resource.sass, gulp.series('sass','index'));
    gulp.watch(resource.pug.src, gulp.series('pug','index'));
    //Refresh when save js file
    gulp.watch("app/common/**/*.js").on('change', browserSync.reload);
    callback();
});

gulp.task('css-bootstrap', (callback)=>{
    gulp.src('node_modules/bootstrap/scss/bootstrap.scss')
        .pipe(sass())
        .pipe(gulp.dest(resource.assets.lib));
    callback();
});

gulp.task('js-bootstrap', (callback)=>{
    gulp.src(['node_modules/bootstrap/dist/js/bootstrap.min.js','node_modules/jquery/dist/jquery.min.js'])
            .pipe(gulp.dest(resource.common.lib));
    callback();
});
//Copy bootstrap file to lib
gulp.task('bootstrap', gulp.series('css-bootstrap','js-bootstrap'), (callback)=>{
    callback();
});

gulp.task('fontawesome', (callback)=>{
    gulp.src('node_modules/@fortawesome/fontawesome-free/**/*')
        .pipe(gulp.dest(resource.assets.fonts.fontawesome));
    callback();
});

gulp.task('font-pacifico', (callback)=>{
    gulp.src('node_modules/typeface-pacifico/files/*')
        .pipe(gulp.dest(resource.assets.fonts.pacifico));
    callback();
});

gulp.task('font-nunito', (callback)=>{
    gulp.src('node_modules/typeface-nunito/files/*')
        .pipe(gulp.dest(resource.assets.fonts.nunito));
    callback();
});
//Copy fonts to assets
gulp.task('fonts', gulp.series('fontawesome', 'font-pacifico', 'font-nunito'), (callback)=>{
    callback();
});

gulp.task('wow-css', (callback)=>{
    gulp.src('node_modules/wow.js/css/libs/animate.css')
        .pipe(gulp.dest(resource.assets.lib));
    callback();
});

gulp.task('wow-js', (callback)=>{
    gulp.src('node_modules/wow.js/dist/wow.min.js')
        .pipe(gulp.dest(resource.common.lib));
    callback();
});
//Copy wow to lib
gulp.task('wow', gulp.series('wow-css','wow-js'), (callback)=>{
    callback();
});


/** Create server*/
gulp.task('browserSync', (callback)=>{
    browserSync.init({
        server:{
            baseDir: 'app'
        }
    });
    callback();
});

gulp.task('default', gulp.series('sass','pug','bootstrap','fonts', 'wow','index', gulp.parallel('browserSync', 'watch')), (callback)=>{
})


